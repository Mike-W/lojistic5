@extends('layouts.flat')
@section('meta')
    @include('layouts.inc.flat-meta', [
    'title'       => $title,
    'description' => $description,
    'keywords'    => $keywords,
    ])
@overwrite
@section('content')
{{--    {!! Html::style('css/blog.css?v='.v()) !!}--}}
    <section id="blog">
        <div class="container">
            <div class="col-sm-7">
                <h1 class="entry-title text-center">
                    {!! $post->post_title  !!}
                </h1>
                @include('parts.blog.author')
                <article>
                    {!! str_replace('/blog/wp-content/uploads/', '/wp-content/uploads/', $post->post_content)  !!}
                </article>
            </div>
            <div class="col-sm-4 col-sm-offset-1">
                @include('parts.blog-sidebar')
            </div>
        </div>
    </section>
@stop
