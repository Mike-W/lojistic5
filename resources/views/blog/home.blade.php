@extends('layouts.flat')
@section('meta')
    @include('layouts.inc.flat-meta', [
    'title'       => $title,
    'description' => $description,
    'keywords'    => 'parcel audit, Lojistic, parcel contracts, parcel auditing, parcel audit services, parcel auditing companies, parcel audit companies, freight audit, Lojistic, freight contracts, freight auditing, freight audit services, freight auditing companies, freight audit companies'
    ])
@overwrite
@section('styles')
    @if(count(Request::segments())!= 1)
        {{--<meta name="robots" content="noindex, follow" />--}}
    @endif
    @if($pagination['prev'] != '#')
        <link rel="prev" href="{{ $pagination['prev'] }}"/>
    @endif
    @if($pagination['next'] != '#')
        <link rel="next" href="{{ $pagination['next'] }}"/>
    @endif
@stop
@section('content')
    <section id="blog">
        <div class="container">
            <div class="col-sm-7">
                <h1 id='blog-title'>{{ $h1 }}</h1>
                @foreach ($blogContent as $post)
                    <div id="post-{{ $post->ID }}" class="post type-post status-publish format-standard hentry">
                        <h2 class="entry-title">
                            <a href="/blog/{{ $post->post_name }}" title="Permalink to {{ $post->post_title }}"
                               rel="bookmark" class="entry-title">
                                {!! $post->post_title !!}
                            </a>
                        </h2>
                        @include('parts.blog.author')
                        <div class="entry-content">
                            <p>
                                {!! strip_tags(substr($post->post_content, 0, 400))  !!}...
                            </p>

                            <p class="continue-reading text-right">
                                <a href="/blog/{{ $post->post_name }}" class="btn btn-primary">
                                    Continue reading
                                    <span class="meta">→</span>
                                </a>
                            </p>
                        </div>
                        <!-- .entry-content -->
                    </div>
                    <hr>
                @endforeach
            </div>
            <div class="col-sm-4 col-sm-offset-1">
                @include('parts.blog-sidebar')
            </div>
            @if(!Request::is('blog/category/*'))
                <div class="row">
                    <div class="col-sm-12">
                        <nav>
                            <ul class="pagination pagination-lg">
                                <li {{$pagination['prev'] == '#' ? 'class="disabled"':''}}>
                                    <a href="{{ $pagination['prev'] }}" aria-label="Previous">
                                        <span aria-hidden="true">Prev</span>
                                    </a>
                                </li>
                                @if($pagination['page'] > 5)
                                    <li><a href="{{ url('blog/page/1') }}">1</a></li>
                                    <li class="disabled"><a href="#">...</a></li>
                                @endif
                                @if($pagination['page'] > 3)
                                    <li>
                                        <a href="{{ url('blog/page/'.($pagination['page']-3) ) }}">{{$pagination['page']-3}}</a>
                                    </li>
                                    <li>
                                        <a href="{{ url('blog/page/'.($pagination['page']-2) ) }}">{{$pagination['page']-2}}</a>
                                    </li>
                                    <li>
                                        <a href="{{ url('blog/page/'.($pagination['page']-1) ) }}">{{$pagination['page']-1}}</a>
                                    </li>
                                @elseif($pagination['page'] > 2)
                                    <li>
                                        <a href="{{ url('blog/page/'.($pagination['page']-2) ) }}">{{$pagination['page']-2}}</a>
                                    </li>
                                    <li>
                                        <a href="{{ url('blog/page/'.($pagination['page']-1) ) }}">{{$pagination['page']-1}}</a>
                                    </li>
                                @elseif($pagination['page'] > 1)
                                    <li>
                                        <a href="{{ url('blog/page/'.($pagination['page']-1) ) }}">{{$pagination['page']-1}}</a>
                                    </li>
                                @endif
                                <li class="active"><a href="#">{{$pagination['page']}}</a></li>
                                @if($pagination['pagesLeft'] > 3)
                                    <li>
                                        <a href="{{ url('blog/page/'.($pagination['page']+1) ) }}">{{$pagination['page']+1}}</a>
                                    </li>
                                    <li>
                                        <a href="{{ url('blog/page/'.($pagination['page']+2) ) }}">{{$pagination['page']+2}}</a>
                                    </li>
                                    <li>
                                        <a href="{{ url('blog/page/'.($pagination['page']+3) ) }}">{{$pagination['page']+3}}</a>
                                    </li>
                                @elseif($pagination['pagesLeft'] > 2)
                                    <li>
                                        <a href="{{ url('blog/page/'.($pagination['page']+1) ) }}">{{$pagination['page']+1}}</a>
                                    </li>
                                    <li>
                                        <a href="{{ url('blog/page/'.($pagination['page']+2) ) }}">{{$pagination['page']+2}}</a>
                                    </li>
                                    <li>
                                        <a href="{{ url('blog/page/'.($pagination['page']+3) ) }}">{{$pagination['page']+3}}</a>
                                    </li>
                                @elseif($pagination['pagesLeft'] > 1)
                                    <li>
                                        <a href="{{ url('blog/page/'.($pagination['page']+1) ) }}">{{$pagination['page']+1}}</a>
                                    </li>
                                    <li>
                                        <a href="{{ url('blog/page/'.($pagination['page']+2) ) }}">{{$pagination['page']+2}}</a>
                                    </li>
                                @elseif($pagination['pagesLeft'] > 0)
                                    <li>
                                        <a href="{{ url('blog/page/'.($pagination['page']+1) ) }}">{{$pagination['page']+1}}</a>
                                    </li>
                                @endif
                                @if($pagination['pagesLeft'] > 4)
                                    <li class="disabled"><a href="#">...</a></li>
                                    <li>
                                        <a href="{{ url('blog/page/'.($pagination['totalPages']) ) }}">{{$pagination['totalPages']}}</a>
                                    </li>
                                @endif
                                <li {{ $pagination['next'] == '#' ? 'class="disabled"' : '' }}>
                                    <a href="{{ $pagination['next'] }}" aria-label="Next">
                                        <span aria-hidden="true">Next</span>
                                    </a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
            @endif
        </div>
    </section>
@stop
