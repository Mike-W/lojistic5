<div class="col-md-2 col-xs-5 tool">
    <a  href="{{ $url }}">
        <i class="navy eloji {{ $eloji }} fs-72"></i>
    </a>
    <a  href="{{ $url }}">
        {!! $label  !!}
    </a>
</div>