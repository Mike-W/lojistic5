@include('parts.snippets.define-logo-svg')


<section class="navy np">
    <div class="container">
        <nav>
            <div class="col-sm-4 col-xs-12 np">
                <a class="white uppercase" href="https://www.lojistic.com/greater-good">
                   Greater Good Collective
                </a>
            </div>
            <div class="minimal-nav-right text-right hidden-xs fs-16 col-xs-12 col-sm-8">
                <span class="white uppercase" href="#">giving back to the community </span>
                &nbsp; | &nbsp;
                    <span class="white" href="#">
                        <i class="eloji-phone fs-20">&nbsp;</i>Call Us: 800-783-5753
                    </span>
            </div>
        </nav>
    </div>
</section>
<!-- end nav -->
