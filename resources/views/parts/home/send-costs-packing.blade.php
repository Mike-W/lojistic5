<!--  Intro  -->
<section class="showcase utah">
    <video controls="" autoplay="" loop="" name="media">
        <source src="https://giant.gfycat.com/HatefulImpureEelelephant.webm" type="video/webm">
    </video>
    <div class="container text-center">
        <h1 class="inline text-shadow white">
            LOJISTIC
        </h1>
        <p class="fw-bold fs-24 text-shadow white">
            ...that feeling you get when you save on shipping costs!
        </p>

        <a class="btn btn-md btn-info" href="/lojistic">OUR STORY</a>
        <a class="btn btn-md btn-info" href="/videos">SWEET VIDEOS</a>
        {{--<a class="btn btn-md btn-info" href="/parcel">PARCÈL LEBOX</a>--}}
        <a class="btn btn-md btn-info" href="/blog">OUR BLOG</a><br>
        <a class="btn btn-md btn-info" href="/free-shipping-resources">FREE SHIPPING RESOURCES</a>
    </div>
</section>
<!--  End Intro  -->
